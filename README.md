# Simple Sub Shell Self Timer

Created to time yourself as you do certain tasks and show
your time after you exit.

## Install

```
go get -u github.com/robmuh/timeme
```
