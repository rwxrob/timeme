package main

import (
	"fmt"
	"os"
	"path"
	"time"
)

func check(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	cwd, err := os.Getwd()
	check(err)

	os.Setenv("EMOJI", "⏱️ ")
	os.Setenv("TIMEME", "1")

	pa := os.ProcAttr{
		Files: []*os.File{os.Stdin, os.Stdout, os.Stderr},
		Dir:   cwd,
	}

	var start time.Time
	var stop time.Time

	start = time.Now()
	fmt.Println("Starting timer (exit to stop).")

	shell := os.Getenv("SHELL")
	name := path.Base(shell)
	proc, err := os.StartProcess(shell, []string{name}, &pa)
	check(err)

	proc.Wait()

	stop = time.Now()
	fmt.Printf("%v\n", stop.Sub(start))
}
